#include "knapsack.h"
#include <math.h>
#include <algorithm>

int bestBenefit = 0;

//////////////////////////////////////Para cambiar de algoritmo alternar los comentarios entre las líneas 20-24//////////////////////////////////////

main (){
  int amount, weight, res;
  cin >> amount;
  cin >> weight;
  if(amount != 0){
    vector<int> weights(amount);
    vector<int> values(amount);
    for (int i = 0; i < amount; i++) {
      cin >> weights[i];
      cin >> values[i];
    }
    //res = knapsackD(weight, amount, weights, values);
    //res = knapsackB2(weight, amount, weights, values, 0, 0);
    //res = knapsackB1(weight, amount, weights, values, 0, 0);
    //res = knapsackB(weight, amount, weights, values, 0, 0);
    res = knapsackBF(weight, amount, weights, values);
    bestBenefit = 0;
    cout << res << endl;
  } else {
    cout << 0 << endl;
  }
  return 0;
}


void printV(vector<int> &V){
  int size = V.size();
  cout << "[";
  for (int i = 0; i < size; i++) {
    cout << V[i] << ",";
  }
  cout << "]";
}

void printM(vector<vector<int> > &M){
  int size = M.size();
  for (int i = 0; i < size; i++) {
    printV(M[i]);
    cout << endl;
  }
}

int max(int a, int b){
  if (a > b){
    return a;
  } else {
    return b;
  }
}

int sum(vector<int> &values, int current){
  int i = 0;
  while(current < values.size()){
    i = i + values[current];
    current++;
  }
  return i;
}

//////////////////////////////////////Dinamica//////////////////////////////////////
int knapsackD(int weight, int amount, vector<int> &weights, vector<int> &values){
  int i, j;
  int zeroW = 0;
  for (int i = 0; i < weights.size(); i++) {
    zeroW = weights[i] == 0 ? zeroW + values[i] : zeroW;
  }
  vector<vector<int> > M(amount+1, vector<int>(weight+1, 0));
  for (int i = 0; i <= amount; i++) {
    for (int j = 0; j <= weight; j++) {
      if (i == 0 || j == 0){
        M[i][j] = 0;
      } else if (weights[i-1] <= j){
        M[i][j] = weights[i-1] == 0 ? M[i-1][j] : max(values[i-1] + M[i-1][j-weights[i-1]], M[i-1][j]);
      } else{
        M[i][j] = M[i-1][j];
      }
    }
  }
  return M[amount][weight] + zeroW;
}



//////////////////////////////////////Backtracking//////////////////////////////////////
int knapsackB(int weight, int amount, vector<int> &weights, vector<int> &values, int benefit, int current){
  int i, j, k;
  if (current == amount){
    return weight < 0 ? 0 : benefit;
  }
  i = knapsackB(weight-weights[current], amount, weights, values, benefit+values[current], current+1);
  j = knapsackB(weight, amount, weights, values, benefit, current+1);

  k = max(i, j);
  return k;
}

//////////////////////////////////////BacktrackingPoda2//////////////////////////////////////
int knapsackB2(int weight, int amount, vector<int> &weights, vector<int> &values, int benefit, int current){
  int i, j, k;
  if (weight < 0){
    return 0;
  }

  if (current == amount){
    return benefit;
  }
  i = knapsackB2(weight-weights[current], amount, weights, values, benefit+values[current], current+1);
  j = knapsackB2(weight, amount, weights, values, benefit, current+1);

  k = max(i, j);
  return k;
}


//////////////////////////////////////BacktrackingPoda1//////////////////////////////////////
int knapsackB1(int weight, int amount, vector<int> &weights, vector<int> &values, int benefit, int current){
  int i, j, k;
  if (current == amount){
    if(benefit > bestBenefit && !(weight < 0)){
      bestBenefit = benefit;
      return benefit;
    } else {
      return 0;
    }
  }
  if (benefit + sum(values, current) < bestBenefit){
    return 0;
  }
  i = knapsackB1(weight-weights[current], amount, weights, values, benefit+values[current], current+1);
  j = knapsackB1(weight, amount, weights, values, benefit, current+1);

  k = max(i, j);
  return k;
}

//////////////////////////////////////FuerzaBruta//////////////////////////////////////
int knapsackBF(int weight, int amount, vector<int> &weights, vector<int> &values){
  long int k = pow(2, amount);
  vector<int> permutation(amount+1, 0);
  int topBenefit = 0;
  for (int i = 0; i < k; i++) {
    int j = amount-1;
    int currentW = 0;
    int currentV = 0;
    while(permutation[j]!= 0 && j > 0){
      permutation[j] = 0;
      j--;
    }
    permutation[j] = 1;
    for (int l = 0; l < amount; l++) {
      if(permutation[l]==1){
        currentW = currentW + weights[l];
        currentV = currentV + values[l];
      }
    }
    if(currentW <= weight && currentV > topBenefit){
      topBenefit = currentV;
    }
  }
  return topBenefit;
}
