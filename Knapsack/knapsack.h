#include <iostream>
#include <vector>
#include <chrono>

using namespace std;

int knapsackD(int weight, int amount, vector<int> &weights, vector<int> &values);
int knapsackB2(int weight, int amount, vector<int> &weights, vector<int> &values, int benefit, int current);
int knapsackB1(int weight, int amount, vector<int> &weights, vector<int> &values, int benefit, int current);
int knapsackB(int weight, int amount, vector<int> &weights, vector<int> &values, int benefit, int current);
int knapsackBF(int weight, int amount, vector<int> &weights, vector<int> &values);
int max(int a, int b);
int sum(vector<int> &values, int current);

//Auxiliares
void printV(vector<int> &V);
void printM(vector<vector<int> > &M);
