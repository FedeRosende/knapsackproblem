#include "tests.h"

using namespace std;
#define ya std::chrono::high_resolution_clock::now

int main(){
  int amount = 25;
  int testsAmount = amount*2;
  while(amount>=0){
    generarCasos(amount, testsAmount);
    amount--;
    testsAmount = amount*2;
  }
  cout << -1 << " " << -1 <<endl;
  return 0;
}

void generarCasos(int amount, int testsAmount){
  int j = 0;
  while(j<testsAmount){
    cout <<  amount << " ";
    int weight = 1 + rand() % (amount*4);
    cout <<  weight << "\n";
    int i = 0;
    while(i<amount){
      int currentW = rand() % (weight*2 + 1);
      cout <<  currentW << " ";
      int currentV = 1 + rand() % 25;
      cout <<  currentV << "\n";
      i++;
    }
    j++;
  }
}
